#!/usr/bin/env python

import requests
import os
import time
import codecs
from operator import itemgetter
from xml.dom.minidom import parse
from jinja2 import Environment, FileSystemLoader

path = os.path.dirname(os.path.realpath(__file__))

def getText(nodelist):
	rc = []
	for node in nodelist:
		if node.nodeType == node.TEXT_NODE:
			rc.append(node.data)
	return ''.join(rc)

urls = {
	'oikotie':'http://tyopaikat.oikotie.fi/pl/rss/9406',
	'monster':'http://rss.jobsearch.monster.com/rssquery.ashx?rad_units=km&cy=FI&pp=20&jt=2&tm=0&sort=dt.rv.di&occ=660.11848&occ=660.11979&occ=660.11996&occ=660.11904&occ=660.11969&occ=660.11787&occ=660.11987&occ=660.11882&occ=660.11772&occ=660.11774&occ=660.11754&occ=660.12005&occ=660.11970&baseurl=nayta.monster.fi',
	'mol':'http://www.mol.fi/paikat/Search.do?lang=fi&searchExecute=true&AreaA1=2&AreaB1=2&AreaB1=20&AreaB1=21&AreaB1=22&AreaB1=23&AreaB1=24&AreaB1=25&AreaB1=26&AreaB1=27&AreaB1=29&professionCode=25&municipalities=&province=---&search=Etsi&country=---&freshness=2&duration=12&type=21&rentalLabour=false&searchphrase=&rss=true&id=6e3cb7bf0a6a020a007ec7da0bdd9dad',
}


jobs = []
for source in urls:
	url = urls[source]
	source_cache = '%s-cache.xml' % source	
	cachefile = os.path.join(path,source_cache)
	#how many hours to cache
	
	max_life = 60*0
	cache_lifetime = time.time() - os.path.getmtime(cachefile)

	if (os.stat(cachefile)[6] == 0) or (cache_lifetime > max_life):
		f = open(cachefile,'wb')	
		rss = requests.get(url)
		try:
			if source == 'monster':
				f.write(rss.text[3:].encode('iso-8859-1'))	
			else:
				if source == 'oikotie':
					f.write(rss.text.encode('utf-8'))
				else:
					f.write(rss.text.encode('iso-8859-1'))
		except UnicodeEncodeError, e:
			print source
			print e

		f.close()

	cache = open(cachefile,'r')
	try:
		xml = parse(cache)
		cache.close()
		items = xml.getElementsByTagName('item')
		for item in items:
			job = {}
			job['title'] = getText(item.getElementsByTagName('title')[0].childNodes)
			job['description'] = getText(item.getElementsByTagName('description')[0].childNodes)
			job['link'] = item.getElementsByTagName('link')[0].childNodes[0].wholeText
			job['source'] = source
			jobs.append(job)
	except Exception, e:
		print 'could not parse %s' % source
		print e
		print cache.readlines()
		cache.close()	


#templating
env = Environment(loader=FileSystemLoader(os.path.join(path,'templates')))
template = env.get_template('template.html')

#sort jobs by title
jobs = sorted(jobs, key=itemgetter('title'))
with codecs.open(os.path.join(path,'htdocs','index.html'),'wb') as output:
	output.write(template.render(jobs=jobs).encode('utf-8'))
	